var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(bodyParser.json());

var caixas = [
  {
    id: 0, nome: 'CAIXA 1', capacidadeTotal: 2000, ultimaLeitura: 1800, controlador: 'CN01',
    historico: [{ dia: 20, consumo: 200, atividades: [{ desc: 'lavado telhado' }, { desc: 'lavado calçada' }] },
    { dia: 21, consumo: 400, atividades: [{ desc: 'lavado carro' }] },
    { dia: 22, consumo: 600, atividades: [{ desc: 'lavado piscina' }] },
    { dia: 23, consumo: 200, atividades: [{ desc: 'descrição' }] },
    { dia: 24, consumo: 190, atividades: [{ desc: 'descrição' }] },
    { dia: 25, consumo: 100, atividades: [{ desc: 'descrição' }] },
    { dia: 26, consumo: 150, atividades: [{ desc: 'descrição' }] },
    { dia: 27, consumo: 220, atividades: [{ desc: 'descrição' }] }]
  },
  {
    id: 1, nome: 'CAIXA 2', capacidadeTotal: 2500, ultimaLeitura: 1200, controlador: 'CN02',
    historico: [{ dia: 20, consumo: 1600, atividades: [{ desc: 'descrição' }] },
    { dia: 21, consumo: 1200, atividades: [{ desc: 'descrição' }] },
    { dia: 22, consumo: 1450, atividades: [{ desc: 'descrição' }] },
    { dia: 23, consumo: 2200, atividades: [{ desc: 'descrição' }] },
    { dia: 24, consumo: 1900, atividades: [{ desc: 'descrição' }] },
    { dia: 25, consumo: 2100, atividades: [{ desc: 'descrição' }] },
    { dia: 26, consumo: 2150, atividades: [{ desc: 'descrição' }] },
    { dia: 27, consumo: 1220, atividades: [{ desc: 'descrição' }] }]
  },
  {
    id: 2, nome: 'CAIXA 3', capacidadeTotal: 3000, ultimaLeitura: 2700, controlador: 'CN03',
    historico: [{ dia: 20, consumo: 6600 },
    { dia: 21, consumo: 4200 },
    { dia: 22, consumo: 3450 },
    { dia: 23, consumo: 5200 },
    { dia: 24, consumo: 3900 },
    { dia: 25, consumo: 3100 },
    { dia: 26, consumo: 1150 },
    { dia: 27, consumo: 6220 }]
  },
  {
    id: 3, nome: 'CAIXA 4', capacidadeTotal: 4000, ultimaLeitura: 2000, controlador: 'CN04',
    historico: [{ dia: 20, consumo: 2600 },
    { dia: 21, consumo: 3200 },
    { dia: 22, consumo: 2450 },
    { dia: 23, consumo: 1200 },
    { dia: 24, consumo: 1900 },
    { dia: 25, consumo: 1100 },
    { dia: 26, consumo: 1150 },
    { dia: 27, consumo: 1220 }]
  },
  {
    id: 4, nome: 'CAIXA 5', capacidadeTotal: 5000, ultimaLeitura: 1000, controlador: 'CN05',
    historico: [{ dia: 20, consumo: 400 },
    { dia: 21, consumo: 3200 },
    { dia: 22, consumo: 2450 },
    { dia: 23, consumo: 200 },
    { dia: 24, consumo: 1900 },
    { dia: 25, consumo: 3100 },
    { dia: 26, consumo: 3220 },
    { dia: 27, consumo: 1220 }]
  },
  {
    id: 5, nome: 'CAIXA 6', capacidadeTotal: 6000, ultimaLeitura: 1800, controlador: 'CN06',
    historico: [{ dia: 20, consumo: 1600 },
    { dia: 21, consumo: 3200 },
    { dia: 22, consumo: 3350 },
    { dia: 23, consumo: 4200 },
    { dia: 24, consumo: 1900 },
    { dia: 25, consumo: 3100 },
    { dia: 26, consumo: 2150 },
    { dia: 27, consumo: 1220 }]
  }
];

var usuarios = [
  { id: 0, usuario: 'lucas', senha: 'lucas' }
];


app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.get('/caixas', function (req, res) {
  setTimeout(function () {
    res.json(caixas);
  }, 5000)

  //res.json(caixas);
});

app.get('/caixa/:id', function (req, res) {
  caixas.forEach(function (caixa) {
    if (caixa.id == req.params.id) {
      res.json(caixa);
      return;
    }
  });
  res.status(404).end();
});

app.post('/caixas', function (req, res) {
  caixas.push(req.body);
  res.json(true);
});

app.post('/login', function (req, res) {
  usuarios.forEach(function (usuario) {
    if (usuario.usuario == req.body.usuario && usuario.senha == req.body.senha) {
      res.status(200).end();
      return;
    }
  })

  res.status(401).end();
  /*setTimeout(function () {
        res.status(401).end();      
        return;
  }, 5000)*/
});

app.listen(process.env.PORT || 3412);